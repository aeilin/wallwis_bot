import logging
import logging.config
from typing import Optional, Callable, Any
import random
from datetime import datetime, timedelta
import json

import botocore
import boto3
from boto3.dynamodb.conditions import Attr
import telebot
import telebot.formatting
from environs import Env


ERR_MSG = """Sorry, I've some technical difficulties. Please try again later"""
WRNG_MSG = "Sorry, I didn't understand"
WLCM_MSG = (
    "Hi, I am Walls Wisdom Bot.\n"
    "To acquire random Wisdom use /random\n"
    "To acquire Wisdom Of The Day use /daily."
)
UPLD_MSG = "I've uploaded photo, file_id: "


env = Env()

LOG_LEVEL = env.log_level("LOG_LEVEL", "INFO")
logging.config.fileConfig("logging.ini", disable_existing_loggers=True)
logger = logging.getLogger(__name__)
logger.setLevel(LOG_LEVEL)


ADMINS = set(env.list("WALLWIS_BOT_ADMINS_IDS", subcast=int))
TOKEN = env.str("WALLWIS_BOT_TOKEN")

bot = telebot.TeleBot(TOKEN, threaded=False)
_dynamo_db = boto3.resource("dynamodb")


FILE_IDS_TABLE = env.str("WALLWIS_BOT_FILE_IDS_TABLE")
UTILS_TABLE = env.str("WALLWIS_BOT_UTILS_TABLE")

DAILY_CACHE_KEY = "daily"
LATEST_SID_KEY = "latest-sid"

INLINE_BATCH_SIZE = 5


# fmt: off
class WallWisBotError(Exception): ...
class OptimisticLockError(WallWisBotError): ...
# fmt: on


def get_latest_sequential_id(*, dynamodb=_dynamo_db) -> int:
    logger.debug("acquiring latest sequential id")
    table = dynamodb.Table(UTILS_TABLE)
    response = table.get_item(Key={"key": LATEST_SID_KEY})
    logger.debug("latest sequential id received", extra={"response": response})
    return int(response["Item"]["value"])


def get_file_id(sequential_id: int, *, dynamodb=_dynamo_db) -> str:
    logger.debug("acquiring file id", extra={"sid": sequential_id})
    table = dynamodb.Table(FILE_IDS_TABLE)
    response = table.get_item(Key={"sID": str(sequential_id)})
    logger.debug("file id received", extra={"sid": sequential_id, "response": response})
    return response["Item"]["fileID"]


def batch_get_file_ids(sids: list[int], *, dynamodb=_dynamo_db) -> list[str]:
    logger.debug("acquiring batch file id", extra={"sids": sids})
    q = {"Keys": [{"sID": str(sid)} for sid in sids]}
    response = dynamodb.batch_get_item(RequestItems={FILE_IDS_TABLE: q})
    logger.debug("file id received", extra={"sid": sids, "response": response})
    return [r["fileID"] for r in response["Responses"][FILE_IDS_TABLE]]


def _increment_latest_sid(*, dynamodb=_dynamo_db) -> int:
    utils_table = dynamodb.Table(UTILS_TABLE)

    sid = utils_table.get_item(Key={"key": LATEST_SID_KEY})["Item"]
    current_version = sid.get("version", 0)
    sid["value"] += 1
    sid["version"] += 1
    try:
        utils_table.put_item(
            Item=sid, ConditionExpression=Attr("version").eq(current_version)
        )
    except botocore.exceptions.ClientError as err:
        if err.response["Error"]["Code"] == "ConditionalCheckFailedException":
            raise OptimisticLockError from err
        raise
    return int(sid["value"])


def save_file_id(file_id: str, *, dynamodb=_dynamo_db):
    file_ids_table = dynamodb.Table(FILE_IDS_TABLE)

    try:
        sid = _increment_latest_sid()
    except OptimisticLockError:
        logger.error(
            "save file id, lock error", exc_info=True, extra={"file_id": file_id}
        )
        sid = _increment_latest_sid()

    item = {"sID": str(sid), "fileID": file_id}
    file_ids_table.put_item(Item=item)


def get_cache(key: str, dynamodb=_dynamo_db) -> dict[str, Any]:
    logger.debug("requesting cache", extra={"key": key})
    table = dynamodb.Table(UTILS_TABLE)
    item = table.get_item(Key={"key": key}).get("Item", dict())
    logger.debug("cache hit/miss", extra={"key": key, "item": item})
    return item


def set_cache(
    key: str, file_id: str, ttl: Optional[int] = None, *, dynamodb=_dynamo_db
):
    table = dynamodb.Table(UTILS_TABLE)
    item = {"key": key, "value": file_id}
    if ttl:
        item.update({"ttl": str(ttl)})

    logger.debug("setting the cache", extra={"item": item, "key": key, "ttl": ttl})
    table.put_item(Item=item)
    logger.debug("cache set", extra={"item": item, "key": key, "ttl": ttl})


def get_random() -> str:
    latest = get_latest_sequential_id()
    sequential_id = random.randint(0, latest)
    return get_file_id(sequential_id)


def get_n_random(k: int) -> list[str]:
    n = get_latest_sequential_id()
    file_ids = list(random.choices(range(n), k=min(k, n)))
    return batch_get_file_ids(file_ids)


def get_daily():
    # TODO: lock
    ttl = int((datetime.utcnow().date() + timedelta(1)).strftime("%s"))

    cached = get_cache(DAILY_CACHE_KEY)
    if cached and int(cached["ttl"]) >= ttl:
        return cached["value"]

    daily = get_random()
    set_cache(DAILY_CACHE_KEY, daily, ttl)
    logger.info("daily set", extra={"file_id": daily, "ttl": ttl})

    return daily


_is_admin: Callable[[telebot.types.Message], bool] = lambda message: message.from_user.id in ADMINS  # fmt: skip
@bot.message_handler(func=_is_admin, chat_types=["private"], content_types=["photo"])
def upload_handler(message: telebot.types.Message):
    log_extra = {"from": message.from_user, "chat": message.chat}
    logger.info("upload handler", extra=log_extra)

    file_id, max_fs = None, -float("inf")
    for photo in message.photo:
        if photo.file_size > max_fs:
            max_fs = photo.file_size
            file_id = photo.file_id

    try:
        save_file_id(file_id)
    except:
        logger.error(
            "upload handler save_file_id error", exc_info=True, extra=log_extra
        )
        bot.reply_to(message, ERR_MSG)
    else:
        spoilered_file_id = telebot.formatting.mspoiler(file_id)
        bot.reply_to(message, f"{UPLD_MSG} {spoilered_file_id}")


@bot.message_handler(commands=["random"])
def random_handler(message):
    log_extra = {"from": message.from_user, "chat": message.chat, "rtype": "command"}
    logger.info("/random start", extra=log_extra)

    try:
        photo = get_random()
    except:
        logger.error("/random", extra=log_extra, exc_info=True)
        bot.reply_to(message, ERR_MSG)
    else:
        bot.send_photo(message.chat.id, photo=photo)
        logger.info("/random sent", extra={**log_extra, "file_id": photo})


@bot.message_handler(commands=["daily"])
def daily_handler(message: telebot.types.Message):
    log_extra = {"from": message.from_user, "chat": message.chat, "rtype": "command"}
    logger.info("/daily start", extra=log_extra)

    try:
        daily = get_daily()
    except:
        logger.error("/daily", extra=log_extra, exc_info=True)
        bot.reply_to(message, ERR_MSG)
    else:
        bot.send_photo(message.chat.id, photo=daily)


@bot.message_handler(commands=["help", "start"])
def welcome_handler(message: telebot.types.Message):
    bot.reply_to(message, WLCM_MSG)


@bot.message_handler(func=lambda message: True)
def default_handler(message: telebot.types.Message):
    logger.info(
        "default handler",
        extra={
            "from": message.from_user,
            "content_type": message.content_type,
            "chat": message.chat,
        },
    )
    bot.reply_to(message, WRNG_MSG)


@bot.inline_handler(lambda q: True)
def inline_handler(query: telebot.types.InlineQuery):
    log_extra = {
        "from": query.from_user,
        "chat_type": query.chat_type,
        "rtype": "inline",
        "query": query.query,
    }
    logger.info("inline start", extra=log_extra)

    try:
        if query.query == "daily":
            r = get_daily()
            results = [telebot.types.InlineQueryResultCachedPhoto(r, r)]
        else:
            rs = get_n_random(INLINE_BATCH_SIZE)
            results = [telebot.types.InlineQueryResultCachedPhoto(r, r) for r in rs]
    except:
        logger.error("inline", extra=log_extra, exc_info=True)
    else:
        bot.answer_inline_query(query.id, results)


def process_event(event, *, bot=bot):
    request_body_dict = json.loads(event["body"])
    update = telebot.types.Update.de_json(request_body_dict)
    bot.process_new_updates([update])


def handler(event, context):
    process_event(event)
    return {"statusCode": 200}
