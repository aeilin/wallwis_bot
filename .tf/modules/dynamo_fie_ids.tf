resource "aws_dynamodb_table" "wallwis_bot_file_ids_table" {
  name           = var.wallwis_bot_file_ids_table_name
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "sID"

  attribute {
    name = "sID"
    type = "S"
  }
}
