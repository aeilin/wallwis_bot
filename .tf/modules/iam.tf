data "aws_iam_policy_document" "lambda" {
    statement {
        effect = "Allow"
        actions = [
            "sts:AssumeRole",
        ]
        principals {
            type = "Service"
            identifiers = [
                "lambda.amazonaws.com"
            ]
        }
    }
}

data "aws_iam_policy_document" "wallwis_bot" {
    statement {
        effect = "Allow"
        actions = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
        ]
        resources = [
            "${aws_cloudwatch_log_group.wallwis_bot.arn}:*"
        ]
    }
    statement {
        effect = "Allow"
        actions = [
                "dynamodb:Scan",
                "dynamodb:GetItem",
                "dynamodb:BatchGetItem",
                "dynamodb:PutItem",
                "dynamodb:UpdateItem"
        ]
        resources = [
            "${aws_dynamodb_table.wallwis_bot_utils_table.arn}",
            "${aws_dynamodb_table.wallwis_bot_file_ids_table.arn}",
        ]
    }
}

resource "aws_iam_role" "wallwis_bot" {
   name                 = "${var.env}-${local.wallwis_bot_name}"
   assume_role_policy   = data.aws_iam_policy_document.lambda.json
}

resource "aws_iam_role_policy" "wallwis_bot" {
    role    = aws_iam_role.wallwis_bot.id
    policy  = data.aws_iam_policy_document.wallwis_bot.json
}
