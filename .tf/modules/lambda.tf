resource "aws_cloudwatch_log_group" "wallwis_bot" {
    name = "/aws/lambda/${var.env}-${local.wallwis_bot_name}"

    retention_in_days = 7
}

resource "aws_lambda_function" "wallwis_bot" {
   function_name = "${var.env}-${local.wallwis_bot_name}"

   s3_bucket = var.ops_bucket
   s3_key    = "${local.wallwis_bot_name}-${var.build}.zip"

   handler = "lambda.handler"
   runtime = "python3.9"

   role = aws_iam_role.wallwis_bot.arn

   environment {
       variables = {
            WALLWIS_BOT_TOKEN           = var.telegram_token
            WALLWIS_BOT_FILE_IDS_TABLE  = var.wallwis_bot_file_ids_table_name
            WALLWIS_BOT_UTILS_TABLE     = var.wallwis_bot_utils_table_name
            WALLWIS_BOT_ADMINS_IDS      = var.wallwis_bot_admins_ids
            LOG_LEVEL                   = var.log_level
       }
   }

   depends_on = [
       aws_cloudwatch_log_group.wallwis_bot
    ]
}


resource "aws_lambda_function_url" "wallwis_bot_url" {
  function_name      = aws_lambda_function.wallwis_bot.function_name
  authorization_type = "NONE"
}
