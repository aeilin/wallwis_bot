resource "aws_dynamodb_table" "wallwis_bot_utils_table" {
  name           = var.wallwis_bot_utils_table_name
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "key"

  attribute {
    name = "key"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}
