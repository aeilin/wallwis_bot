variable "ops_bucket" {}

variable "build" {}

variable "env" {}

variable "wallwis_bot_file_ids_table_name" {}

variable "wallwis_bot_utils_table_name" {}

variable "telegram_token" {}

variable "wallwis_bot_admins_ids" {}

variable "log_level" {
    default = "INFO"
}
