module "app" {
  source = "./modules"

  env                             = var.env
  ops_bucket                      = var.ops_bucket
  build                           = var.build
  wallwis_bot_file_ids_table_name = var.wallwis_bot_file_ids_table_name
  wallwis_bot_utils_table_name    = var.wallwis_bot_utils_table_name
  telegram_token                  = var.telegram_token
  wallwis_bot_admins_ids          = var.wallwis_bot_admins_ids
}
