variable "ops_bucket" {}

variable "env" {}

variable "build" {}

variable "region" {
    default = "eu-central-1"
}

variable "wallwis_bot_file_ids_table_name" {
    default = "wallwis-bot-file-ids"
}

variable "wallwis_bot_utils_table_name" {
    default = "wallwis-bot-utils"
}

variable "telegram_token" {}

variable "wallwis_bot_admins_ids" {}
